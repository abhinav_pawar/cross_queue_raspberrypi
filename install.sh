#!/bin/bash
ROOM=$1
ENV=$2
if [ -z "${ROOM}" ]
then
echo "ROOM is required in first parameter"
exit 3
fi

if [ -z "${ENV}" ]
then
echo "ENV is not provided default PRODUCTION will be set"
fi
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
echo $SCRIPTPATH
AUTOSTART="@""$SCRIPTPATH""/cross_queue.sh"
echo $AUTOSTART
File=/etc/xdg/lxsession/LXDE-pi/autostart
sudo grep -qF -- "$AUTOSTART" "$File" || sudo echo "$AUTOSTART" >> "$File"
sed -i '2s/.*/'ROOM=''${ROOM}'/' cross_queue.sh
#ex -s -c '2i|ROOM='"${ROOM}" -c x cross_queue.sh
if [ -z "${ENV}" ]
then
echo "Not setting env to cross_queue"
else
sed -i '3s/.*/'ENV=''${ENV}'/' cross_queue.sh	
#ex -s -c '3i|ENV='"${ENV}" -c x cross_queue.sh
fi
$SCRIPTPATH/cross_queue.sh
exit 0
#etc.
