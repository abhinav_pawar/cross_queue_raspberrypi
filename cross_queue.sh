#!/bin/bash
ROOM=abc
ENV=abc
MAC=00:00:00:00
ORGANISATION_ID=abc
PRACTITIONER_ID=abc
TYPE=abc
if [ -e /sys/class/net/eth0 ]; then
    MAC=$(cat /sys/class/net/eth0/address)
elif [ -e /sys/class/net/enx* ]; then
    MAC=$(cat /sys/class/net/enx*/address)
else
    MAC=$(cat /sys/class/net/wlan0/address)
fi
echo $MAC


#ROOM=$1
#ENV=$2
BASE=https://cross.crosshealthsolutions.com/v1/room/$ROOM

if [ -z "${ENV}" ]
then
echo "ENV is not provided default PRODUCTION will be set"
ENV="prod"
fi

if [ -z "${ROOM}" ]
then
echo "ROOM is required. Exiting..."
exit 3
fi

if [ $ENV == "dev" ]
then
BASE=https://dev.cross.crosshealthsolutions.com/v1/room/$ROOM
elif [ $ENV == "stage" ]
then
BASE=https://stage.cross.crosshealthsolutions.com/v1/room/$ROOM
elif [ $ENV == "prod" ]
then
BASE=https://cross.crosshealthsolutions.com/v1/room/$ROOM
else
BASE=https://cross.crosshealthsolutions.com/v1/room/$ROOM
fi

echo $BASE
echo $ENV
echo $ROOM



function open_web_browser {
#organisationId=$1
#practitionerId=$2
#type=$3
url="organization_id=${ORGANISATION_ID}&practitioner_id=${PRACTITIONER_ID}&status=${TYPE}"
echo "$url"
organisationId="$organisationId" | tr -d '"'
echo "Opening chromium with organisation id $ORGANISATION_ID with practitionerId $PRACTITIONER_ID"
if [ $ENV == "dev" ]
then
SAKRA_BASE=https://dev.sakra.rgcross.com/tv?${url}
elif [ $ENV == "stage" ]
then
SAKRA_BASE=https://stage.sakra.rgcross.com/tv?${url}
elif [ $ENV == "prod" ]
then
SAKRA_BASE=https://sakra.rgcross.com/tv?${url}
else
SAKRA_BASE=https://sakra.rgcross.com/tv?${url}
fi
chromium-browser --start-fullscreen --disable-session-crashed-bubble --disable-infobars --no-sandbox --app=${SAKRA_BASE}
exit 0
}




statusCode=500
function api_hit {
resp="$(curl -sb -H  "Accept: application/json" -H "Content-Type: application/json" $BASE | jq '.data[0]' )"
echo "${resp}"
org_id="$( echo ${resp} | jq -r '.organization_id')"
#pract_id="$( echo $resp | jq -r '.occupants[0].practitioner_id')"
occupants="$( echo $resp | jq -r '.occupants')"
type="$( echo $resp | jq -r '.queue_status')"
echo "${org_id}"
echo "${pract_id}"
echo "Loop start"
if [ $type == "ASSESSMENT" ]
then
for row in $(echo "${occupants}" | jq -r '.[] | @base64'); do
    _jq() {
     echo ${row} | base64 --decode | jq -r ${1}
    }
	if [ -z "${pract_id}" ]
	then
	pract_id="$(_jq '.practitioner_id')"
	else
	pract_id="${pract_id},$(_jq '.practitioner_id')"
	fi	
done
else
pract_id="$( echo $resp | jq -r '.occupants[0].practitioner_id')"
fi
echo "Loop end"
echo "${pract_id}"
echo "Pract id"
if [  -z "${org_id}"  ]
then
echo "Organization id not found"
else
ORGANISATION_ID=$org_id
PRACTITIONER_ID=$pract_id
TYPE=$type
echo "${ORGANISATION_ID}"
open_web_browser
fi
}

function start_player {
echo "${statusCode}"

while [ $statusCode != 200 ]
do 
statusCode=$(curl -s -o /dev/null -w ''%{http_code}'' $BASE)
if [ $statusCode != 200 ]
then
sleep 2
else
echo "${statusCode}"
api_hit
fi
done
}
start_player
#etc.
